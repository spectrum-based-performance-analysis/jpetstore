#
#    Copyright 2010-2016 the original author or authors.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

mvn clean package
mvn cargo:run
cp -R target/cargo/configurations/tomcat8x/* target/cargo/installs/apache-tomcat-8.5.6/apache-tomcat-8.5.6 
echo "CLASSPATH=sbppl.jar:../../../../../jpetstore/WEB-INF/classes:../../../../../jpetstore/WEB-INF/lib/stripes-1.6.0.jar:../../../../../jpetstore/WEB-INF/lib/spring-tx-4.3.3.RELEASE.jar:../../../../../jpetstore/WEB-INF/lib/spring-web-4.3.3.RELEASE.jar:../../../../../jpetstore/WEB-INF/lib/spring-jdbc-4.3.3.RELEASE.jar:../../../../../jpetstore/WEB-INF/lib/mybatis-spring-1.3.0.jar:../../../../../jpetstore/WEB-INF/lib/spring-context-4.3.3.RELEASE.jar:../../../../../jpetstore/WEB-INF/lib/spring-beans-4.3.3.RELEASE.jar:../../../../../jpetstore/WEB-INF/lib/spring-expression-4.3.3.RELEASE.jar:../../../../../jpetstore/WEB-INF/lib/spring-aop-4.3.3.RELEASE.jar:../../../../../jpetstore/WEB-INF/lib/spring-core-4.3.3.RELEASE.jar" > target/cargo/installs/apache-tomcat-8.5.6/apache-tomcat-8.5.6/bin/setenv.sh
echo "CATALINA_OPTS=\"-javaagent:sbppl.jar\"" >> target/cargo/installs/apache-tomcat-8.5.6/apache-tomcat-8.5.6/bin/setenv.sh
cp /home/flo/workspaces/java/sbppl/bin/sbppl.jar target/cargo/installs/apache-tomcat-8.5.6/apache-tomcat-8.5.6/bin/

SBPPL_CONF=target/cargo/installs/apache-tomcat-8.5.6/apache-tomcat-8.5.6/bin/sbppl.conf
echo "version=v1" > $SBPPL_CONF
echo "runIdentifier=add runidentifier" >> $SBPPL_CONF
echo "spectrumType=CountSpectrum" >> $SBPPL_CONF
echo "includes=org.mybatis.jpetstore" >> $SBPPL_CONF
echo "excludes=sun;java;com.sun;sbppl.instrumentation;sbppl.spectrum" >> $SBPPL_CONF
echo "savePath=spectra" >> $SBPPL_CONF
mkdir target/cargo/installs/apache-tomcat-8.5.6/apache-tomcat-8.5.6/bin/spectra


